#!/bin/bash

# register gitlab runner
gitlab-runner register

# install and run service
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start

# sleep forever
sleep infinity
